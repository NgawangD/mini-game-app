import React from 'react'
import { View, Text, Image, StyleSheet, Dimensions, useWindowDimensions, ScrollView} from 'react-native'
import PrimaryButton from '../components/ui/PrimaryButton'
import Title from '../components/ui/Title'
import Colors from '../constants/Colors'


export default function GameOverScreen({
    roundsNumber,
    userNumber,
onStartNewGame,
}) {
    const {width, height} = useWindowDimensions()

    return (
        // <ScrollView style={{ flex: 1}}>
            <View style={styles.rootContainer}>
            <Title>Game Over!</Title>
            <View style={[styles.imageContainer, {width: height < 480 ? 150 : 300, height: height < 480 ? 150 : 300,
            borderRadius: height< 380 ? 100 : 200}]}>
                <Image style={styles.image}
source={require('../assets/success.png')} />
        </View>
        <Text style={styles.summaryText}>
            Your phone needed <Text
style={styles.highlight}>{roundsNumber}</Text>{' '}
            rounds to guess the number{' '}
        <Text style={styles.highlight}>{userNumber}</Text>
    </Text>
    <PrimaryButton onPress={onStartNewGame}>Start New
Game</PrimaryButton>
        </View>
        // </ScrollView>
    )
}

// const deviceWidth = Dimensions.get('window').width
const styles = StyleSheet.create({
rootContainer: {
    flex: 1,
    padding: 24,
    alignItems: 'center',
    justifyContent: 'center',
},
imageContainer: {
    // width: deviceWidth < 380 ? 18 : 36,
    // height: deviceWidth < 380 ? 18 : 36,
    
    borderWidth: 3,
    borderColor: Colors.primary800,
    overflow: 'hidden',
    margin: 36,
},
image: {
    height: '100%',
    width: '100%',
},
highlight: {
    fontFamily: 'opensansbold',
    color: Colors.primary500,
},
summaryText: {
    fontFamily: 'opensans',
    fontSize: 24,
    textAlign: 'center',
    marginBottom: 24,
},
})